#language: ru

@exportscenarios

Функционал: Загрузка бюджетной классификации

Как Тестировщик
Я хочу загрузить бюджетную классификацию
Чтобы обеспечить контекст выполнения других тестов

Контекст: 
	Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий


Сценарий: я загружаю бюджетную классификацию

	Когда     В командном интерфейсе я выбираю "Планирование и санкционирование" "Помощник обновления бюджетной классификации"
	Тогда открылось окно "Помощник обновления бюджетной классификации"
	И	  я буду выбирать внешний файл "F:\git\test\data\federal.clax"
	И     я нажимаю на кнопку "Открыть файл…"
	Тогда открылось окно "Помощник обновления бюджетной классификации"
	И     я перехожу к закладке "Доступные обновления"
	Тогда открылось окно "Помощник обновления бюджетной классификации"
	И     я перехожу к закладке "Страницы помощника"
	Тогда открылось окно "Помощник обновления бюджетной классификации"
	Тогда открылось окно "Помощник обновления бюджетной классификации"
	И     в таблице "ДоступныеОбновления" я нажимаю на кнопку "Установить флажки доступных обновлений"
	Тогда открылось окно "Помощник обновления бюджетной классификации"
	И     я нажимаю на кнопку "&Далее >"

	И     пауза 2000
#	35 минут!

	Тогда открылось окно "Помощник обновления бюджетной классификации"
	И     я нажимаю на кнопку "Закрыть"
