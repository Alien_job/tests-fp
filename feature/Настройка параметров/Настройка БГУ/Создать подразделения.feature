#language: ru

@exportscenarios

Функционал: Создание подразделений

Как Тестировщик
Я хочу иметь возможность запустить создание подразделений
Чтобы обеспечить контекст для других тестов 

Контекст: 
	Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий


Сценарий: я создаю подразделения Администрация и Библиотека и Гараж и Общежитие

	Когда в командном интерфейсе я выбираю "Справочники" "Подразделения"

	Тогда открылось окно "Подразделения"
	И     в таблице "Список" я нажимаю на кнопку "Создать"
	Тогда открылось окно "Подразделение (создание)"
	И     я выбираю значение реквизита "Организация" из формы списка
	Тогда открылось окно "1С:Предприятие"
	И     я нажимаю на кнопку "Да"
	Тогда открылось окно "Организации"
	И     в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Образовательное учреждение' |
	И     в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Подразделение (создание) *"
	И     в поле "Наименование" я ввожу текст "Администрация"
	И     я нажимаю на кнопку "Записать и закрыть"

	Тогда открылось окно "Подразделения"
	И     в таблице "Список" я нажимаю на кнопку "Создать"
	Тогда открылось окно "Подразделение (создание)"
	И     я выбираю значение реквизита "Организация" из формы списка
	Тогда открылось окно "1С:Предприятие"
	И     я нажимаю на кнопку "Да"
	Тогда открылось окно "Организации"
	И     в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Образовательное учреждение' |
	И     в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Подразделение (создание) *"
	И     в поле "Наименование" я ввожу текст "Библиотека"
	И     я нажимаю на кнопку "Записать и закрыть"

	Тогда открылось окно "Подразделения"
	И     в таблице "Список" я нажимаю на кнопку "Создать"
	Тогда открылось окно "Подразделение (создание)"
	И     я выбираю значение реквизита "Организация" из формы списка
	Тогда открылось окно "1С:Предприятие"
	И     я нажимаю на кнопку "Да"
	Тогда открылось окно "Организации"
	И     в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Образовательное учреждение' |
	И     в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Подразделение (создание) *"
	И     в поле "Наименование" я ввожу текст "Гараж"
	И     я нажимаю на кнопку "Записать и закрыть"

	Тогда открылось окно "Подразделения"
	И     в таблице "Список" я нажимаю на кнопку "Создать"
	Тогда открылось окно "Подразделение (создание)"
	И     я выбираю значение реквизита "Организация" из формы списка
	Тогда открылось окно "1С:Предприятие"
	И     я нажимаю на кнопку "Да"
	Тогда открылось окно "Организации"
	И     в таблице "Список" я перехожу к строке:
		| 'Наименование' |
		| 'Образовательное учреждение' |
	И     в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Подразделение (создание) *"
	И     в поле "Наименование" я ввожу текст "Общежитие"
	И     я нажимаю на кнопку "Записать и закрыть"

