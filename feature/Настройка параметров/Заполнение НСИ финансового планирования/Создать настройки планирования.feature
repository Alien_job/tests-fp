#language: ru

@exportscenarios

#Используются данные "Создать аналитические разрезы планирования"

Функционал: Создание настроек планирования

Как Тестировщик
Я хочу Создать настройки планрования  
Чтобы проверить работу создания настроек планирования и обеспечить контекст для других тестов 

Контекст: 
	Дано Я запускаю сценарий открытия TestClient или подключаю уже существующий


Сценарий: я создаю настройки планирования

	Когда     В командном интерфейсе я выбираю "Финансовое планирование" "Настройки планирования"
	Тогда открылось окно "Настройки планирования"
	И     я нажимаю на кнопку "Создать"
	Тогда открылось окно "Настройки планирования (создание)"
	И     в таблице "ПрименяемыеРазрезыАналитики" я нажимаю на кнопку "Добавить"
	И     в таблице "ПрименяемыеРазрезыАналитики" я выбираю значение реквизита "Разрез аналитики" из формы списка
	Тогда открылось окно "Аналитические разрезы планирования"
	И     в таблице "Список" я перехожу к строке:
		| 'Код'       | 'Наименование' |
		| '000000102' | 'Номенклатура' |
	И     в таблице "Список" я активизирую поле "Наименование"
	И     в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Настройки планирования (создание) *"
	И     в таблице "ПрименяемыеРазрезыАналитики" я завершаю редактирование строки
	И     в таблице "ПрименяемыеРазрезыАналитики" я нажимаю на кнопку "Добавить"
	И     в таблице "ПрименяемыеРазрезыАналитики" я выбираю значение реквизита "Разрез аналитики" из формы списка
	Тогда открылось окно "Аналитические разрезы планирования"
	И     в таблице "Список" я перехожу к строке:
		| 'Код'       | 'Наименование' |
		| '000000103' | 'Произвольный' |
	И     в таблице "Список" я активизирую поле "Наименование"
	И     в таблице "Список" я выбираю текущую строку
	Тогда открылось окно "Настройки планирования (создание) *"
	И     в таблице "ПрименяемыеРазрезыАналитики" я завершаю редактирование строки
	И     я нажимаю на кнопку "Записать и закрыть"
	Тогда открылось окно "Настройки планирования"
	И     я нажимаю на кнопку "Создать"
	Тогда открылось окно "Настройки планирования (создание)"
	И     в таблице "ПрименяемыеРазрезыАналитики" я нажимаю на кнопку "Добавить"
	И     в таблице "ПрименяемыеРазрезыАналитики" из выпадающего списка "Разрез аналитики" я выбираю "Номенклатура"
	И     в таблице "ПрименяемыеРазрезыАналитики" я завершаю редактирование строки
	И     я нажимаю на кнопку "Записать и закрыть"
	Тогда открылось окно "Настройки планирования"
	И     я нажимаю на кнопку "Создать"
	Тогда открылось окно "Настройки планирования (создание)"
	И     в таблице "ПрименяемыеРазрезыАналитики" я нажимаю на кнопку "Добавить"
	И     в таблице "ПрименяемыеРазрезыАналитики" из выпадающего списка "Разрез аналитики" я выбираю "Произвольный"
	И     в таблице "ПрименяемыеРазрезыАналитики" я завершаю редактирование строки
	И     я нажимаю на кнопку "Записать и закрыть"
	Тогда открылось окно "Настройки планирования"
	И     я нажимаю на кнопку "Создать"
	Тогда открылось окно "Настройки планирования (создание)"
	И     в таблице "ПрименяемыеРазрезыАналитики" я нажимаю на кнопку "Добавить"
	И     в таблице "ПрименяемыеРазрезыАналитики" из выпадающего списка "Разрез аналитики" я выбираю "Номенклатура"
	И     в таблице "ПрименяемыеРазрезыАналитики" я завершаю редактирование строки
	И     в таблице "ПрименяемыеРазрезыАналитики" я добавляю новую строку
	И     в таблице "ПрименяемыеРазрезыАналитики" из выпадающего списка "Разрез аналитики" я выбираю "Произвольный"
	И     в таблице "ПрименяемыеРазрезыАналитики" я завершаю редактирование строки
	И     я нажимаю кнопку выбора у поля "Периодичность планирования"
	И     из выпадающего списка "Периодичность планирования" я выбираю "Квартал"
	И     я нажимаю на кнопку "Записать и закрыть"
